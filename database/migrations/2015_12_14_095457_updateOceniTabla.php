<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOceniTabla extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('studoceni', function($table){
            $table->integer('predmeti_id')->unsigned();
            $table->foreign('predmeti_id')->references('id')->on('predmeti');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('studoceni', function($table){

            $table->dropForeign('predmeti_id');
            $table->dropColumn('predmeti_id');


        });
    }
}

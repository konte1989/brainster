<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class UserTableSeeder extends Seeder {

    public function run(){
        DB::table('users')->delete();

        User::create(array(

            'ime' => 'Ime-1',
            'prezime' => 'Prezime-1',
            'email' => 'Email-1',
            'lozinka' => Hash::make('Lozinka-1'),
        ));
    }
}
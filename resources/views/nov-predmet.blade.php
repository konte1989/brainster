@extends('top')
@extends('navbar')
@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading" style="text-align: center"><h1>Креирај нов предмет:</h1></div>
                    <div class="panel-body">

                        @if(Session::has('message'))
                            <div class="alert alert-success">
                                {{ Session::get('message') }}
                            </div>
                        @endif

                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('/predmeti') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                @IF(Auth::user()->studentprof == 'profesor' || Auth::user()->studentprof == 'admin'  )
                                <div class="form-group">
                                    {!!  Form::label('predmet', 'Предмет' , ['class' => 'col-md-4 control-label']) !!}
                                    <div class="col-md-6">
                                        {!! Form::text('predmet', null, ['class' => 'form-control'])  !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!!  Form::label('profesor', 'Професор' , ['class' => 'col-md-4 control-label']) !!}
                                    <div class="col-md-6">
                                        {!! Form::select('prof_stud_id', $profesori, null, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Додади
                                        </button>
                                    </div>
                                </div>
                                </div>
                            </form>

                            <div class="panel-body">
                                <form class="form-horizontal" role="form" method="POST" action="{{ url('/predmeti') }}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    @ELSEIF(Auth::user()->studentprof == 'student')

                                    {!!  Form::label('predmet_stud', 'Предмет' , ['class' => 'col-md-4 control-label']) !!}
                                    <div class="col-md-6">
                                        {!! Form::select('prof_stud_id_predmet', $predmeti, null, ['class' => 'form-control']) !!}
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                Запиши предмет
                                            </button>
                                        </div>
                                    </div>

                                @ENDIF
                                </form>
                            </div>


                        </div></div></div></div></div>
@endsection

@include('bottom')
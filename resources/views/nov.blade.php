@extends('top')
@extends('navbar')
@section('content')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><h2>Регистрација на @IF($array['samoProfesor'] == 'Profesor' && $array['samoStudent'] == '') нов професор:</h2></div>
                            @ELSEIF($array['samoStudent'] == 'Student' && $array['samoProfesor'] == '')
                            нов студент: </h2></div>
                    @endif
                    <div class="panel-body">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Грешка!</strong> Имаше проблем при внесување на податоците!<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach

                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal" role="form" method="POST" @IF($array['samoProfesor'] == 'Profesor' && $array['samoStudent'] == '')
                        action="{{ url('/profesori') }}">
                            @ELSE($array['samoStudent'] == 'Student' && $array['samoProfesor'] == '')
                            action="{{ url('/studenti') }}">
                            @ENDIF
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Име</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="ime" value="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Презиме</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="prezime" value="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Email адреса</label>
                                        <div class="col-md-6">
                                            <input type="email" class="form-control" name="email" value="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Лозинка</label>
                                        <div class="col-md-6">
                                            <input type="password" class="form-control" name="password">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Потврдна лозинка</label>
                                        <div class="col-md-6">
                                            <input type="password" class="form-control" name="password_confirmation">
                                        </div>
                                    </div>

                            @IF ($array['samoProfesor'] == 'Profesor' && $array['samoStudent'] == '')

                                      <div class="form-group">
                                          <label class="col-md-4 control-label" id="plataTxt" >Плата:</label>
                                          <div class="col-md-6">
                                              <input type="text" class="form-control" name="plata" id="plata" value=""><br>
                                          </div>
                                      </div>



                            @ELSEIF ($array['samoStudent'] == 'Student' && $array['samoProfesor'] == '')

                                <div class="form-group">
                                    <label class="col-md-4 control-label" id="studGodTxt" >Студентска година: </label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="studGod" id="studGod" value="">
                                    </div>
                                </div>


                            @ENDIF

                                       <div class="radio col-md-4">
                                            <div class="col-md-6">
                                                <input type="radio" style="display: none;" id="student" onclick="check()" name="studprof" value="student" > <label style="display: none;">Студент</label>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="radio" style="display: none;" id="prof" onclick="check()" name="studprof" value="profesor"> <label style="display: none;">Професор</label>
                                            </div>
                                        </div>





                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                Додади
                                            </button>
                                        </div>
                                    </div>
                                </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@include('bottom')


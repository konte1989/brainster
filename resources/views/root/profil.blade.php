@extends('top')
@extends('navbar')
@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading" style="text-align: center"><h1>Профил на {!! $currentUser['ime'] . " " . $currentUser['prezime'] !!}</h1></div>
                    <div class="panel-body">

                        @if(Session::has('message'))
                            <div class="alert alert-success">
                                {{ Session::get('message') }}
                            </div>
                        @endif

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Грешка!</strong> Имаше проблем при внесување на податоците!<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach

                                </ul>
                            </div>
                        @endif


                        {!! Form::model($currentUser, array('method' => 'PATCH', 'class' => 'form-horizontal', 'route' => array('profil.update', $currentUser->id))) !!}
                        <div class="form-group">
                            {!!  Form::label('ime', 'Име' , ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::text('ime', $currentUser['ime'], ['class' => 'form-control'])  !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!!  Form::label('prezime', 'Презиме', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::text('prezime', $currentUser['prezime'], ['class' => 'form-control'])  !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('email', 'Емаил', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::email('email', $currentUser['email'], ['class' => 'form-control'])  !!}
                            </div>

                        </div>

                        <div class="form-group">
                            {!!  Form::label('password', 'Лозинка', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::password('password', ['class' => 'form-control'])  !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!!  Form::label('password_confirmation', 'Потврдна лозинка', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::password('password_confirmation', ['class' => 'form-control'])  !!}
                            </div>

                        </div>

                        <div class="form-group">
                        <div class="col-md-3" >
                            {!! Form::submit('Измени профил', ['class'=>'btn btn-primary']) !!}
                            {!! Form::close() !!}
                        </div>

                        <div class="col-md-3">
                            {!! Form::open(array('method' => 'GET', 'route' => '/')) !!}
                            {!! Form::submit('Назад', array('class' => 'btn btn-warning')) !!}
                            {!! Form::close() !!}
                        </div>
                            </div>

                        {!! Form::close() !!}

                    </div></div></div></div></div>


@stop

@extends('bottom')

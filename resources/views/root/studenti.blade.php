@extends('top')
@extends('navbar')
@section('content')


    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading" style="text-align: center;"><h1>Листа на студенти</h1></div>
                    <div class="panel-body">

                        @if(Session::has('message'))
                            <div class="alert alert-success">
                                {{ Session::get('message') }}
                            </div>
                        @endif
                        @IF(Auth::user()->studentprof == 'admin')
                        <form action="" method="get">
                            <ul class="list-group">
                                @foreach($studenti as $student)

                                    <li class="list-group-item" id="{!! $student['id'] !!}"><a href="{{ route('studenti.edit', $student->id) }}">{{ $student['ime']. ' ' . $student['prezime'] }}</a></li><br>
                                    @IF(Auth::user()->studentprof == 'admin')
                                        @if($student['activation'] == '0')

                                            {!! Form::open(array('method' => 'PATCH', 'route' => array('studenti.update', $student->id) )) !!}
                                            {!! Form::submit('Активирај', array('name' => 'activation', 'class' => 'btn btn-info')) !!}
                                            {!! Form::close() !!}

                                        @elseif($student['activation'] == '1')
                                            {!! Form::open(array('method' => 'PATCH', 'route' => array('studenti.update', $student->id) )) !!}
                                            {!! Form::submit('Деактивирај', array('name' => 'activation', 'class' => 'btn btn-danger')) !!}
                                            {!! Form::close() !!}
                                        @endif
                                @ENDIF
                                @endforeach
                            </ul>
                        </form>
                            @ENDIF
                        @IF(Auth::user()->studentprof == 'admin')
                        <div class="form-group">
                            <div class="col-md-4">
                                {!! Form::open(array('method' => 'GET', 'route' => 'studenti.create')) !!}
                                {!! Form::submit('Креирај нов студент', array('class' => 'btn btn-success')) !!}
                                {!! Form::close() !!}
                            </div>
                        @ENDIF
                            @IF(Auth::user()->studentprof == 'student' || Auth::user()->studentprof == 'profesor' )
                            <form action="" method="get">
                            @foreach($predmeti as $predmet)
                                <li class="list-group-item" id="{!! $predmet['id'] !!}"><a href="{{ route('/lista', $predmet->id) }}">{{ $predmet['predmet']}}</a></li><br>
                                </form>
                            @endforeach
                            @ENDIF

                            <div class="col-md-3">
                                {!! Form::open(array('method' => 'GET', 'route' => '/')) !!}
                                {!! Form::submit('Назад', array('class' => 'btn btn-warning')) !!}
                                {!! Form::close() !!}
                            </div>
                        </div>

                    </div></div></div></div></div>

@stop

@extends('bottom')

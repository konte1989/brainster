@extends('top')
@extends('navbar')
@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading" style="text-align: center"><h1>Листа на предмети</h1></div>
                    <div class="panel-body">

                        @if(Session::has('message'))
                            <div class="alert alert-success">
                                {{ Session::get('message') }}
                            </div>
                        @endif


                        <form action="" method="get">
                            <ul class="list-group">

                                @foreach($predmeti as $predmet)

                                    @IF(Auth::user()->studentprof == 'admin' || Auth::user()->studentprof == 'student')
                                    <li class="list-group-item" id="{!! $predmet['id'] !!}"><a href="{{ route('predmeti.edit', $predmet->id) }}">{{$predmet['predmet'] }}</a></li><br>

                                    @ELSEIF(Auth::user()->studentprof == 'profesor')
                                    <li class="list-group-item" id="">{{$predmet['predmet'] }}</li><br>
                                    @ENDIF

                                @endforeach
                            </ul>
                            </form>

                        @IF(Auth::user()->studentprof == 'student')
                            {!! Form::open(array('method' => 'GET', 'route' => array('predmeti.create'))) !!}
                            {!! Form::submit('Запиши предмет', array('name' => 'activation', 'class' => 'btn btn-info')) !!}
                            {!! Form::close() !!}
                        @ENDIF

                            @IF(Auth::user()->studentprof == 'admin')
                            <div class="form-group">
                                <div class="col-md-4">
                                    {!! Form::open(array('method' => 'GET', 'route' => 'predmeti.create')) !!}
                                    {!! Form::submit('Креирај нов предмет', array('class' => 'btn btn-success')) !!}
                                    {!! Form::close() !!}
                                </div>
                            @ENDIF

                                <div class="col-md-3">
                                    {!! Form::open(array('method' => 'GET', 'route' => '/')) !!}
                                    {!! Form::submit('Назад', array('class' => 'btn btn-warning')) !!}
                                    {!! Form::close() !!}
                                </div>
                            </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@extends('bottom')

@extends('top')
@extends('navbar')


@section('content')


    @if (Session::has('loginMessage'))
        <div class="alert alert-success">{{ Session::get('loginMessage') }}</div>
    @endif

@stop

@extends('bottom')
@extends('top')
@extends('navbar')
@section('content')


    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading" style="text-align: center"><h1>Листа на професори</h1></div>
                    <div class="panel-body">

                    @if(Session::has('message'))
                        <div class="alert alert-success">
                            {{ Session::get('message') }}
                        </div>
                    @endif


                    <form action="" method="get">
                        <ul class="list-group">
                        @foreach($profesori as $profesor)

                            <li class="list-group-item" id="{!! $profesor['id'] !!}">
                    <a href="{{ route('profesori.edit', $profesor->id) }}">{{ $profesor['ime']. ' ' . $profesor['prezime'] }}</a>
                                @IF(Auth::user()->studentprof == 'admin')
                                    @if($profesor['activation'] == '0')

                                        {!! Form::open(array('method' => 'PATCH', 'route' => array('profesori.update', $profesor->id) )) !!}
                                        {!! Form::submit('Активирај', array('name' => 'activation', 'class' => 'btn btn-info')) !!}
                                        {!! Form::close() !!}

                                    @elseif($profesor['activation'] == '1')
                                        {!! Form::open(array('method' => 'PATCH', 'route' => array('profesori.update', $profesor->id) )) !!}
                                        {!! Form::submit('Деактивирај', array('name' => 'activation', 'class' => 'btn btn-danger')) !!}
                                        {!! Form::close() !!}
                                    @endif
                                @endif

                            @endforeach
                        </ul>
                    </form>

        <div class="form-group">
            <div class="col-md-4">
            {!! Form::open(array('method' => 'GET', 'route' => 'profesori.create')) !!}
            {!! Form::submit('Креирај нов професор', array('class' => 'btn btn-success')) !!}
            {!! Form::close() !!}
            </div>
            <div class="col-md-3">
            {!! Form::open(array('method' => 'GET', 'route' => '/')) !!}
            {!! Form::submit('Назад', array('class' => 'btn btn-warning')) !!}
            {!! Form::close() !!}
            </div>
        </div>

</div></div></div></div></div>

@stop

@extends('bottom')

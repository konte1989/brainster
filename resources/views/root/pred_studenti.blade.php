@extends('top')
@extends('navbar')
@section('content')


    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading" style="text-align: center;"><h1>Листа на студенти</h1></div>
                    <div class="panel-body">

                        @if(Session::has('message'))
                            <div class="alert alert-success">
                                {{ Session::get('message') }}
                            </div>
                        @endif

                            @foreach($predStudenti as $predStud)
                                <li class="list-group-item" id="">{!! $predStud['predmet'] !!}
                                @foreach($predStudenti as $predStud)
                                    <ul>
                                        <li class="list-group-item" id="">{!! $predStud['celoime'] !!}</li><br>
                                    </ul>
                                 @endforeach
                                </li><br>
                            @endforeach


                    </div></div></div></div></div>

@stop

@extends('bottom')
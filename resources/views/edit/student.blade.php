@extends('top')
@extends('navbar')
@section('content')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading" style="text-align: center;"><h2>Измени студент</h2></div>
                    <div class="panel-body">

                        @if(Session::has('message'))
                            <div class="alert alert-success">
                                {{ Session::get('message') }}
                            </div>
                        @endif

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Грешка!</strong> Имаше проблем при внесување на податоците!<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach

                                </ul>
                            </div>
                        @endif

                <div class="panel-body">
                    {!! Form::model($studenti, array('method' => 'PATCH', 'class' => 'form-horizontal', 'route' => array('studenti.update', $studenti->id))) !!}

                        <div class="form-group">
                            {!!  Form::label('ime', 'Име' , ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::text('ime', null, ['class' => 'form-control'])  !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!!  Form::label('prezime', 'Презиме', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                            {!! Form::text('prezime', null, ['class' => 'form-control'])  !!}
                                </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('email', 'Емаил', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::email('email', null, ['class' => 'form-control'])  !!}
                            </div>

                        </div>

                        <div class="form-group">
                            {!!  Form::label('password', 'Лозинка', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::password('password', ['class' => 'form-control'])  !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!!  Form::label('password_confirmation', 'Потврдна лозинка', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::password('password_confirmation', ['class' => 'form-control'])  !!}
                            </div>

                        </div>

                         <div class="form-group">

                            <div class="col-md-3" >
                            {!! Form::submit('Измени профил', ['class'=>'btn btn-primary']) !!}
                            {!! Form::close() !!}
                            </div>

                         <div class="col-md-2" >
                             {!! Form::open(array('method' => 'DELETE', 'route' => array('studenti.destroy', $studenti->id))) !!}
                             {!! Form::submit('Избриши', array('class' => 'btn btn-danger', 'id' => 'delete')) !!}
                             {!! Form::close() !!}
                         </div>
                         <div class="col-md-3">
                             {!! Form::open(array('method' => 'GET', 'url' => '/studenti')) !!}
                             {!! Form::submit('Назад', array('class' => 'btn btn-warning')) !!}
                             {!! Form::close() !!}
                         </div>


                        </div>

                      {!! Form::close() !!}


                    </div>
@STOP
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@include('bottom')
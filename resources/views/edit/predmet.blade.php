@extends('top')
@extends('navbar')
@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading" style="text-align: center"><h1>Измени предмет</h1></div>
                    <div class="panel-body">

                        @if(Session::has('message'))
                            <div class="alert alert-success">
                                {{ Session::get('message') }}
                            </div>
                        @endif
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <strong>Грешка!</strong> Имаше проблем при внесување на податоците!<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach

                                    </ul>
                                </div>
                            @endif

                            <div class="panel-body">

                                {!! Form::model($predmeti, array('method' => 'PATCH', 'class' => 'form-horizontal', 'route' => array('predmeti.update', $predmeti->id))) !!}

                                <div class="form-group">
                                    {!!  Form::label('predmet', 'Предмет' , ['class' => 'col-md-4 control-label']) !!}
                                    <div class="col-md-6">
                                        {!! Form::text('predmet', null, ['class' => 'form-control'])  !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    {!!  Form::label('profesor', 'Професор' , ['class' => 'col-md-4 control-label']) !!}
                                    <div class="col-md-6">
                                    {!! Form::select('prof_stud_id', $profesori, null, ['class' => 'form-control']) !!}
                                    </div>
                                </div>

                                <div class="form-group">

                                    <div class="col-md-4" >
                                        {!! Form::submit('Измени предмет', ['class'=>'btn btn-primary']) !!}
                                        {!! Form::close() !!}
                                    </div>

                                    <div class="col-md-4" >
                                        {!! Form::open(array('method' => 'DELETE', 'route' => array('predmeti.destroy', $predmeti->id))) !!}
                                        {!! Form::submit('Избриши', array('class' => 'btn btn-danger', 'id' => 'delete')) !!}
                                        {!! Form::close() !!}
                                    </div>

                                    <div class="col-md-4">
                                        {!! Form::open(array('method' => 'GET', 'url' => '/predmeti')) !!}
                                        {!! Form::submit('Назад', array('class' => 'btn btn-warning')) !!}
                                        {!! Form::close() !!}
                                    </div>

                                </div>
                            </div>
                            {!! Form::close() !!}

                    </div></div></div></div></div></div>


@stop

@extends('bottom')

@extends('top')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h1>Логирај се</h1>
                    </div>
                    <div class="panel-body">

                        @if (Session::has('logoutMessage'))
                            <div class="alert alert-success">{{ Session::get('logoutMessage') }}</div>
                        @endif

                        @if(Session::has('error'))
                            <div class="alert-box success">
                                <h4>{{ Session::get('error') }}</h4>
                            </div>
                        @endif

                {!! Form::open(array('url' => '/auth/login'))  !!}
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="controls">
                    {!! Form::text('email','',array('id'=>'','class'=>'form-control span6','placeholder' => 'Емаил'))  !!}
                    <p class="errors">{{$errors->first('email')}}</p>
                </div>

                <div class="controls">
                    {!! Form::password('password',array('class'=>'form-control span6', 'placeholder' => 'Лозинка'))  !!}
                    <p class="errors">{{$errors->first('password')}}</p>
                </div>

                <div class="form-group">
                    <div class="col-md-8">
                    <p>{!! Form::submit('Најави се', array('class'=>'btn btn-primary'))  !!}</p>
                    </div>
                    {!! Form::close() !!}
                    <div class="col-md-1">
                    <a href="{{ URL::route('/auth/register') }}" class="btn btn-primary">Регистрирај се</a>
                    </div>
                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@stop

@extends('bottom')
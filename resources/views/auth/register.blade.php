@extends('top')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><h2>Регистрација на нов корисник:</h2></div>
                    <div class="panel-body">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Грешка!</strong> Имаше проблем при внесување на податоците!<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach

                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal" role="form" method="POST" action="/auth/register">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group">
                                <label class="col-md-4 control-label">Име</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="ime" value="{{ old('ime') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Презиме</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="prezime" value="{{ old('prezime') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Email адреса</label>
                                <div class="col-md-6">
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Лозинка</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Потврдна лозинка</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password_confirmation">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="radio col-md-4">
                                    <div class="col-md-6">
                                        <input type="radio" id="student" onclick="check()" name="studprof" value="student" > Студент
                                    </div>
                                    <div class="col-md-6">
                                        <input type="radio" id="prof" onclick="check()" name="studprof" value="profesor"> Професор
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4" id="studGodTxt" style="display: none;">Студентска година: </label>
                                <div class="col-md-6">
                                    <input type="text" name="studGod" id="studGod" style="display: none;">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4" id="plataTxt" style="display: none;">Плата:</label>
                                <div class="col-md-6">
                                    <input type="text" name="plata" id="plata" style="display: none;"><br>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Регистрирај се
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@include('bottom')

<script type="text/javascript">

    function check(){
        if(document.getElementById('student').checked) {
            document.getElementById('student').setAttribute("checked", "checked");
            document.getElementById("studGod").style.display='block';
            document.getElementById("studGodTxt").style.display='block';
            document.getElementById("plata").style.display='none';
            document.getElementById("plataTxt").style.display='none';

        }   else if(document.getElementById('prof').checked) {
            document.getElementById('prof').setAttribute("checked", "checked");
            document.getElementById("studGod").style.display='none';
            document.getElementById("studGodTxt").style.display='none';
            document.getElementById("plata").style.display='block';
            document.getElementById("plataTxt").style.display='block';
        }
    }


</script>
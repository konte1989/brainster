<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dobredojdovte</title>

    {!! Html::style('css/app.css') !!}
    {!! Html::script('js/jquery.min.js') !!}
    {!! Html::script('js/bootstrap.min.js') !!}
    {!! Html::script('js/custom_javascript.js') !!}

</head>
<body>

    <div class="container">
        @yield('content')
    </div>


<script>

    $('div.alert').delay(3000).slideUp(300);

    var limit = 5;
    $('input.single-checkbox').on('change', function(evt) {
        if($(this).siblings(':checked').length >= limit) {
            this.checked = false;
        }
    });

    $(".nav a").on("click", function(){
        $(".nav").find(".active").removeClass("active");
        $(this).parent().addClass("active");
    });

</script>

</body>


<nav class="navbar navbar-inverse" style="margin-bottom: 0px">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="">ФИНКИ</a>
            </div>


            <ul class="nav navbar-nav">

                <li class=""><a href="{{ route('/home') }}">Почетна</a></li>

                @IF(Auth::user()->studentprof == 'student')
                    <li><a href="{{ url('/predmeti') }}" >Предмети</a></li>

                @ELSEIF(Auth::user()->studentprof == 'profesor')
                    <li><a href="{{ url('/predmeti') }}" >Предмети</a></li>
                    <li><a href="{{ url('/profesori') }}">Професори</a></li>

                @ELSEIF(Auth::user()->studentprof == 'admin')
                    <li><a href="{{ url('/predmeti') }}" >Предмети</a></li>
                    <li><a href="{{ url('/profesori') }}">Професори</a></li>
                @ENDIF

                <li><a href="{{ url('/studenti') }}">Студенти</a></li>

                @IF(Auth::user()->studentprof == 'profesor' || Auth::user()->studentprof == 'student')
                <li><a href="">Коментари</a></li>
                @ENDIF
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">{{ $user['ime']." ". $user['prezime'] }} <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ url('/profil') }}">Измени профил</a></li>
                        <li><a href="{{ url('/auth/logout') }}">Одјава</a></li>
                    </ul>
                </li>
            </ul>


</div>
</nav>
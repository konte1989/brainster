<?php

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');


Route::get('auth/logout', 'Auth\AuthController@getLogout');

Route::get('auth/register', array('as' => '/auth/register', 'uses' => 'Auth\AuthController@getRegister'));
Route::post('auth/register', 'Auth\AuthController@postRegister');

Route::filter('auth', function(){

    if (Auth::guest()) return Redirect::to('auth/login');

});

Route::controllers([
    'password' => 'Auth\PasswordController',
]);

Route::group(array('before' => 'auth'), function(){

    Route::get('/', array('as' => '/', 'uses' => 'HomeController@index'));

    Route::get('/studenti/lista/',array('as' => '/lista', 'uses' => 'StudentiController@showPredmeti'));



    Route::resource('/profesori', 'ProfesoriController');

    Route::resource('/predmeti', 'PredmetiController');

    Route::resource('/profil', 'ProfilController');

    Route::resource('/studenti', 'StudentiController');

    Route::get('/home', array('as' => '/home', 'uses' => 'HomeController@index'));


});




// varuiable via route get back
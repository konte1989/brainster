<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Html;
use App\Predmet;
use App\User;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;




class PredmetiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->user = Auth::user();
        View::share('user', $this->user);
    }

    public function index()
    {


        if(Auth::user()->studentprof == 'profesor')
        {
            $id = Auth::user()->id;
            $predmeti = Predmet::where('prof_stud_id', '=', $id)->get();
        } elseif(Auth::user()->studentprof == 'student'){
            $id = Auth::user()->id;
            $predmeti = Predmet::where('prof_stud_id', '=', $id)->get();
        }else{
            $predmeti = Predmet::all();
        }

        return View::make('root.predmeti')->withPredmeti($predmeti);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $profesori = User::select('id')->where('studentprof','=','profesor')->selectRaw('CONCAT(ime," ",prezime) AS celoime')->lists('celoime', 'id');

        $predmeti = Predmet::where('prof_stud_id', '!=', Auth::user()->id)->lists('predmet', 'predmet');

        return View::make('nov-predmet')
            ->withProfesori($profesori)
            ->withPredmeti($predmeti);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        if(isset($_POST['prof_stud_id_predmet']) && !empty($_POST['prof_stud_id_predmet'])){

            $zaspisPredmet = new Predmet();
            $zaspisPredmet->predmet =  $_POST['prof_stud_id_predmet'];
            $zaspisPredmet->prof_stud_id = Auth::user()->id;
            $zaspisPredmet->save();

        }

            $rules = array(
                'predmet' => 'required|unique:predmeti|max:255',
                'prof_stud_id' => 'required|max:255'
            );

            $validator = Validator::make(Input::all(), $rules);

            if ($validator->fails()) {
                return Redirect::to('predmeti/create')
                    ->withErrors($validator);
            } else {

                $predmet = new Predmet();
                $predmet->predmet          = Input::get('predmet');
                $predmet->prof_stud_id      = Input::get('prof_stud_id');
                $predmet->save();

                Session::flash('message', 'Предметот е успешно креиран');
                return Redirect::to('predmeti');
            }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $predmeti = Predmet::findOrFail($id);
        $studenti = User::all()->where('id', '=', $id);

        if (isset($_POST['zapisPredmet']) && !empty($_POST['zapisPredmet'])){

            $predmeti->fill([
                'prof_stud_id' => Auth::user()->id,
            ])->save();

            Session::flash('message', 'Предметот е запишан');
            return redirect()->back();

        } else{

                $profesori = User::select('id')->where('studentprof','=','profesor')->selectRaw('CONCAT(ime," ",prezime) AS celoime')->lists('celoime', 'id');
                    }

        return view('edit.predmet')
                ->with('predmeti', $predmeti)
                ->with('profesori', $profesori)
                ->with('studenti', $studenti);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $predmeti = Predmet::findOrFail($id);

        $input = $request->all();

        $predmeti->fill([
            'predmet'       => $input['predmet'],
            'prof_stud_id'   => $input['prof_stud_id'],
        ])->save();

        Session::flash('message', 'Предметот е успешно изменет');
        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Predmet::find($id)->delete();
        return Redirect::route('predmeti.index');
    }
}

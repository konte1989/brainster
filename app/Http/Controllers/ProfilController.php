<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
//use Illuminate\Support\Facades\Auth;
//use Illuminate\Support\Facades\View;
use Illuminate\Html;
use App\User;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class ProfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->user = Auth::user();
        View::share('user', $this->user);
    }

    public function index()
    {

        $currentUser = Auth::user();

        return View::make('root.profil')->with('currentUser', $currentUser);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(

            'ime' => 'required|max:255',
            'prezime' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required|min:6',
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('profil.create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {

            $currentUser = new User;
            $currentUser->ime = Input::get('ime');
            $currentUser->prezime = Input::get('prezime');
            $currentUser->email = Input::get('email');
            $currentUser->password = bcrypt(Input::get('password'));
            $currentUser->save();

            Session::flash('message', 'Профилот е успешно изменет');
            return Redirect::to('profil');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $currentUser = User::findOrFail($id);

        return view('root.profil')->withCurrentUser($currentUser);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $currentUser = Auth::user($id);

        $this->validate($request, [

        ]);

        $input = $request->all();

        $currentUser->fill([
            'ime'       => $input['ime'],
            'prezime'   => $input['prezime'],
            'email'     => $input['email'],
            'password'  => bcrypt($input['password'])
        ])->save();

        Session::flash('message', 'Профилот е успешно изменет');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

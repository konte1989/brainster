<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Html;
use App\User;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Predmet;


class StudentiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->user = Auth::user();
        View::share('user', $this->user);
    }


    public function index(){

        $predmeti = Predmet::all();
        $studenti = User::all()->where('studentprof', 'student');

        return View::make('root.studenti')->with('studenti', $studenti)->with('predmeti', $predmeti);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $array = array('samoProfesor' => '', 'samoStudent' => 'Student');

        return View::make('nov')->withArray($array);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(

            'ime' => 'required|max:255',
            'prezime' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required|min:6',
            //'studprof' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('studenti/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {

            $student = new User;
            $student->ime          = Input::get('ime');
            $student->prezime      = Input::get('prezime');
            $student->email        = Input::get('email');
            $student->password     = bcrypt(Input::get('password'));
            $student->studentprof  = 'student';
            $student->studgodina   = Input::get('studGod');
            $student->save();

            Session::flash('message', 'Профилот на студентот е успешно креиран');
            return Redirect::to('studenti');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $profesori = User::findOrFail($id);
        $studenti = User::findOrFail($id);

        return view('edit.student')->withStudenti($studenti)->withProfesori($profesori);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $studenti = User::findOrFail($id);
        $input = $request->all();

        if (isset($input['activation']) && !empty($input['activation']) && $input['activation'] == 'Активирај') {

            $studenti->fill([
                'activation' => 1,
            ])->save();
            Session::flash('message', 'Профилот на студентот е активиран');

        }elseif(isset($input['activation']) && !empty($input['activation']) && $input['activation'] == 'Деактивирај'){

            $studenti->fill([
                'activation' => 0,
            ])->save();

            Session::flash('message', 'Профилот на студентот е деактивиран');

        }elseif(empty($input['activation']) && !isset($input['activation'])){

            $studenti->fill([
                'ime'       => $input['ime'],
                'prezime'   => $input['prezime'],
                'email'     => $input['email'],
                'password'  => bcrypt($input['password']),

            ])->save();

            Session::flash('message', 'Профилот на студентот е успешно изменет');

        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return Redirect::route('studenti.index');
    }

    public function showPredmeti(){

        $predStudenti = DB::select( DB::raw("SELECT predmeti.predmet, CONCAT(ime,' ', prezime) AS celoime FROM predmeti
                                    LEFT JOIN users
                                    ON predmeti.prof_stud_id=users.id
                                    WHERE predmeti.prof_stud_id IN
                                    (SELECT id FROM users WHERE studentprof='student')
                                    ORDER BY predmeti.predmet
                                    "));

        $array = json_decode(json_encode($predStudenti), true);


        return View::make('root.pred_studenti')->with('predStudenti', $array);

    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Html;
use App\User;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;


class ProfesoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->user = Auth::user();
        View::share('user', $this->user);
    }

    public function index()
    {

        $profesori = User::all()->where('studentprof', 'profesor');
        return View::make('root.profesori')->with('profesori', $profesori);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $array = array('samoProfesor' => 'Profesor', 'samoStudent' => '');

        return View::make('nov')->withArray($array);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = array(
            'ime' => 'required|max:255',
            'prezime' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required|min:6',
            //'studprof' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('profesori/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {

            $profesor = new User;
            $profesor->ime          = Input::get('ime');
            $profesor->prezime      = Input::get('prezime');
            $profesor->email        = Input::get('email');
            $profesor->password     = bcrypt(Input::get('password'));
            $profesor->studentprof  = 'profesor';
            $profesor->plata        = Input::get('plata');
            $profesor->save();

            Session::flash('message', 'Профилот на професорот е успешно креиран');
            return Redirect::to('profesori');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $studenti = User::findOrFail($id);
        $profesori = User::findOrFail($id);

        return view('edit.profesor')->withProfesori($profesori)->withStudenti($studenti);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $profesori = User::findOrFail($id);
        $input = $request->all();

        if(isset($input['activation']) && !empty($input['activation']) && $input['activation'] == 'Активирај'){

            $profesori->fill([
                'activation' => 1,
            ])->save();
            Session::flash('message', 'Профилот на професорот е активиран');

        }elseif(isset($input['activation']) && !empty($input['activation']) && $input['activation'] == 'Деактивирај'){

            $profesori->fill([
                'activation' => 0,
            ])->save();

            Session::flash('message', 'Профилот на професорот е деактивиран');

        }elseif(empty($input['activation']) && !isset($input['activation'])){

            $profesori->fill([
                'ime'       => $input['ime'],
                'prezime'   => $input['prezime'],
                'email'     => $input['email'],
                'password'  => bcrypt($input['password']),
            ])->save();

            Session::flash('message', 'Профилот на професорот е успешно изменет');

        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        User::find($id)->delete();
        return Redirect::route('profesori.index');
    }

}

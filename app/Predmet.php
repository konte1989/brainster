<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Predmet extends Model
{
    public $table = "predmeti";
    protected $fillable = ['predmet', 'prof_stud_id'];

    public function predmet(){

        return $this->belongsTo(User::class);
    }

    public function ocena(){

        return $this->hasMany(Ocena::class);
    }
}

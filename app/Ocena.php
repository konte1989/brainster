<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ocena extends Model
{
    public function ocena(){

        return $this->belongsTo(User::class);
    }

    public function predmet(){

        return $this->hasMany(Predmet::class);
    }
}

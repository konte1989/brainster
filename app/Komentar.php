<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    public $table = "komentari";
    protected $fillable = ['komentar', 'prof_stud_id'];

    public function user(){

        return $this->belongsTo(User::class);
    }

    public function comment(){

        return $this->hasMany(Komentar::class);
    }
}

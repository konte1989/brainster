/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.6.17 : Database - brainster
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`brainster` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `brainster`;

/*Table structure for table `komentari` */

DROP TABLE IF EXISTS `komentari`;

CREATE TABLE `komentari` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `komentar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prof_stud_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `komentari_prof_stud_id_foreign` (`prof_stud_id`),
  CONSTRAINT `komentari_prof_stud_id_foreign` FOREIGN KEY (`prof_stud_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `komentari` */

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`migration`,`batch`) values ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2015_12_05_104046_create_boot_table',2),('2015_12_05_104720_create_phones_table',2),('2015_12_05_104748_create_posts_table',2),('2015_12_05_104806_create_comments_table',2),('2015_12_09_222350_create_predmeti_table',3),('2015_12_09_222458_create_studoceni_table',3),('2015_12_09_222516_create_komentari_table',3),('2015_12_13_225822_updateUsersTable',3),('2015_12_14_095457_updateOceniTabla',4);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `predmeti` */

DROP TABLE IF EXISTS `predmeti`;

CREATE TABLE `predmeti` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `predmet` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prof_stud_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `predmeti_prof_stud_id_foreign` (`prof_stud_id`),
  CONSTRAINT `predmeti_prof_stud_id_foreign` FOREIGN KEY (`prof_stud_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `predmeti` */

insert  into `predmeti`(`id`,`predmet`,`prof_stud_id`,`created_at`,`updated_at`) values (1,'Predmet_1_Nov',10,'0000-00-00 00:00:00','2015-12-14 19:24:09'),(2,'Predmet_2',10,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,'Predmet_3',21,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,'Predmet_4',21,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,'Predmet_4',21,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(6,'Predmet_6',22,'2015-12-14 12:39:47','2015-12-14 12:39:47'),(7,'Predmet_7',26,'2015-12-14 12:55:48','2015-12-14 12:55:48'),(15,'Predmet_4',8,'2015-12-14 19:12:50','2015-12-14 19:12:50'),(16,'Predmet_4',8,'2015-12-14 19:12:54','2015-12-14 19:12:54'),(17,'Predmet_1_Nov',8,'2015-12-14 19:12:57','2015-12-14 19:12:57'),(18,'Predmet_7',8,'2015-12-14 19:13:10','2015-12-14 19:13:10'),(19,'Predmet_PON',8,'2015-12-14 19:23:45','2015-12-14 19:23:45'),(20,'Predmet_PON',9,'2015-12-14 19:23:45','2015-12-14 19:23:45'),(21,'Predmet_PON',9,'2015-12-14 19:23:45','2015-12-14 19:23:45');

/*Table structure for table `studoceni` */

DROP TABLE IF EXISTS `studoceni`;

CREATE TABLE `studoceni` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ocenka` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prof_stud_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `predmeti_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `studoceni_prof_stud_id_foreign` (`prof_stud_id`),
  KEY `studoceni_predmeti_id_foreign` (`predmeti_id`),
  CONSTRAINT `studoceni_predmeti_id_foreign` FOREIGN KEY (`predmeti_id`) REFERENCES `predmeti` (`id`),
  CONSTRAINT `studoceni_prof_stud_id_foreign` FOREIGN KEY (`prof_stud_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `studoceni` */

insert  into `studoceni`(`id`,`ocenka`,`prof_stud_id`,`created_at`,`updated_at`,`predmeti_id`) values (4,'8',8,'0000-00-00 00:00:00','0000-00-00 00:00:00',2);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ime` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prezime` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `studentprof` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `plata` int(11) NOT NULL,
  `studgodina` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activation` tinyint(1) NOT NULL DEFAULT '0',
  `max_pred` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`ime`,`prezime`,`email`,`password`,`studentprof`,`plata`,`studgodina`,`remember_token`,`created_at`,`updated_at`,`activation`,`max_pred`) values (8,'Filip','Konteski','fkonteski@gmail.com','$2y$10$/HBbIZ58d1Wah8z2gTbvZejAf1aLqINMsrxAYiUBCGVVS2CS4QUr.','student',0,2005,'OycWiv1wHGNG1KYePXSnmi4RbIlLlJnKWtpwTU0L4HdOqr9kKegc5PV6AYi4','2015-12-08 14:02:05','2015-12-14 19:38:11',1,0),(9,'Nori','Gegoska','nori@nori.com','$2y$10$bn6Amsba1YIvM39bJkTR6uuUB3YKe3mdbjWITlQXh6jOx4bNc7jjG','student',0,2007,'787S2VQEImijyyRVgxCLFbSxJ31tsmpgDsz2TorBBUujyqhV2ybyfEn2Dgrr','2015-12-08 18:03:56','2015-12-14 19:38:21',0,0),(10,'Zoran','Asenov','zoran@netra.com.mk','$2y$10$FwPvVcFmyAGCl.BSuRKBi.ornNtPMlkpy8m7/H9p9I1QWEwZsm1py','profesor',2000,0,'cVJHveWmPa54IS6TDYDIBjmy08mYfIQmuMrMvbx9IrvCgtEutgHuM0gp8yTL','2015-12-09 09:17:30','2015-12-14 19:36:35',1,0),(19,'admin','','admin@admin.com','$2y$10$aLUmOsmWJM71aHNJQX2jJ.veLN4pJB1dx6F1H3GbZSfd4gIZyLNBG','admin',0,0,'iAqyyxQLSiqjULz8V9uhwm6l2wwbrvubZrt1Onau1yj1Os9hNdKOxfdpny3H','0000-00-00 00:00:00','2015-12-14 19:38:40',1,0),(21,'Kosta','Nikolovski','kosta@test.com','$2y$10$Fikn2C7sx06xui5worz6gubRRd8qcigDzRO/pqRGxVDmJKRCHNCj2','profesor',1000,0,NULL,'2015-12-10 13:08:17','2015-12-14 19:36:45',0,0),(22,'Profesor_Ime_1','Profesor_Prezime_1','prof1@test.com','$2y$10$/OJy7EioB/aQOJSXCv3NceLUlDxWpJxVQ0bmdy3tBbeq0czlo.RmW','profesor',2000,0,NULL,'2015-12-11 09:44:13','2015-12-14 12:55:55',1,0),(23,'Profesor_Ime_2','Profesor_Prezime_2','prof2@test.com','$2y$10$cRgWT4QHp7fgf5t/Gbio0eF7WNz16zqxVFq0cA6R4ZBLAgXLDnKTi','profesor',3000,0,NULL,'2015-12-11 09:52:03','2015-12-14 02:52:47',0,0),(25,'Student_Ime_2','Student_Prezime_2','student1@test.com','$2y$10$zSl13u.IvLm6TRokw2ZbHesYq.Fj2cBtiwCntba15hGlnRBFJ6SaO','student',0,3000,NULL,'2015-12-11 09:59:13','2015-12-14 12:56:01',1,0),(26,'Korisnik1','Korisnik1','korisnik1@test.com','$2y$10$bn6Amsba1YIvM39bJkTR6uuUB3YKe3mdbjWITlQXh6jOx4bNc7jjG','profesor',99999,0,'ZBfyLGPc6iwW3qLXcpPAXRHy7cMryhZpCaaa6mU6WmAVabvYwpAmRaTzSoaE','2015-12-11 10:00:20','2015-12-14 14:36:21',0,0),(27,'Korisnik2','Korisnik2','korisnik2@test.com','$2y$10$V0GOeHMVDN75Nhx6xpCWcO83H.iQ59zkxiul7KGWyvrbhMRJHOS4S','student',0,2015,'xBDDIZuAvqA1u8KDWfSmyLuUvFAHqjaR0EiDVFoet3DkbQ9qnQKroCIjEE9z','2015-12-11 10:00:51','2015-12-11 10:05:12',0,0),(28,'Dragan','Mushkinja','dragan@netra.com.mk','$2y$10$qzvKBvwb28BmqoHpFVqFxe/d3RHE5fEbgVtFoU/yCO/kg/ClL030S','student',0,2015,NULL,'2015-12-14 12:56:23','2015-12-14 19:38:27',1,0),(29,'Ivana','Gjurevska','ivana@netra.com.mk','$2y$10$RbgKUb9x1HJEWjiQDpL8Ouz9moMAKbEy/eIw3Q6bNsCi2Y/O7NfTu','profesor',3000,0,NULL,'2015-12-14 12:56:47','2015-12-14 19:28:18',0,0),(30,'Mirjan','Konteska','mirjana@test.com','$2y$10$GZ1QD5CSJ2d5cpA5XnxxLeisqabSVc3V4BMc7eB8asONqJF6RCIt6','student',0,2013,NULL,'2015-12-14 13:22:30','2015-12-14 13:22:30',0,0),(31,'Ivan','Papesh','ivan@test.com','$2y$10$cQovTwZ/mdJVIOIvHNZ5bO.H6A4Dg2KP.027oEoUZLbLl7Ve9v8wG','student',0,2003,NULL,'2015-12-14 13:51:39','2015-12-14 13:51:39',0,0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
